USE [DHRDataWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[spValidationCheck_EmployeesDaily]    Script Date: 9/8/2016 10:28:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidationCheck_EmployeesDaily]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	/* Create initial temp table */
	SELECT	E.*, 
			C.Delete_Date as C_Delete_Date,
			0 AS FAILFLAG,	
			GETDATE() as mod_date
	INTO	#TEMP_EMP_VALCHK	
	
	FROM	[DHRDataWarehouse].[dbo].DW_PR_Mast E    
	   JOIN	[DHRDataWarehouse].[dbo].DW_CL_Mast C on (E.Loc_No = C.Loc_No AND E.company = C.company)  
   
	WHERE C.loc_No >= 1000
	  --AND C.Delete_Date is null
	  --AND E.Active_Stat <> 'T' -- only active employees
	;

	/* TODO: Insert the queires below */





	

UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = 0;


/* STATUS IS ??? */
--UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG + 1 WHERE (Active_Stat <> 'T');

/* *** Invalid Birthday *** */
/* BIRTHDATE IS NULL */
UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 2 
WHERE	(Birth_Date is null)
  AND	Active_Stat <> 'T' -- only active employees;
  AND	C_Delete_Date is null

;

/* AGE > 99 */
--UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 4 WHERE (((YEAR(CurDate()) - year(Birth_Date))) > 99);
--UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 4 WHERE (cast(((CurDate() - Birth_Date) / 365) as int) > 99);
UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 4 
WHERE DATEDIFF(year, CAST(Birth_Date AS DATE), CAST(GETDATE() AS DATE)) > 99
  AND	Active_Stat <> 'T' -- only active employees;
  AND	C_Delete_Date is null  

/* AGE < 13 */
--UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 8 WHERE (cast(((CurDate() - Birth_Date) / 365) as int) < 14);
UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 8 
WHERE DATEDIFF(year, CAST(Birth_Date AS DATE), CAST(GETDATE() AS DATE)) < 14
  AND	Active_Stat <> 'T' -- only active employees;
  AND	C_Delete_Date is null  


  /* *** Duplicate PR_ETAX -- ETAX *** */
UPDATE T1 SET FAILFLAG = FAILFLAG | 16 
--SELECT	DUPS.*, T1.*
FROM	#TEMP_EMP_VALCHK T1
    JOIN (

			/* semi original version -- see below for modified version */
			SELECT DISTINCT
			  --:CurrentCompany as CompNo,
			  --E.pkUniqueId,
			  X.company,
			  X.Loc_No,
			  X.Emp_No
			  --,'Employee Tax Options are setup with the same Date and State'

			FROM
			  [DHRDataWarehouse].[dbo].DW_PR_ETAX X
			  
			  
			  /* 
				Note: existing SQL doest not join DW_PR_Mast and DW_CL_Mast on company and location; 
				using #TEMP_EMP_VALCHK will do it automatically, hence the 2 joins below are not necessary (wont hurt if its applied)
			  */
			  --inner join [DHRDataWarehouse].[dbo].DW_CL_Mast C on (C.Loc_No = X.Loc_No AND C.company = X.company)
			  --inner join [DHRDataWarehouse].[dbo].DW_PR_Mast E 
			  inner join #TEMP_EMP_VALCHK E 	
												ON  (E.Loc_No = X.Loc_No 
												AND E.Emp_No = X.Emp_No
												AND E.company = X.company)
			WHERE
				(
				DATEDIFF(YEAR, E.Last_Paid, GETDATE()) <= 2
				OR 
				E.Active_Stat <> 'T'
				)
				/* Note: existing SQL does not apply 'DW_CL_Mast.Delete_Date is null' */
			  --AND E.C_Delete_Date is null
			  
			  AND 
				(
				(NOT EXISTS (
				  SELECT        *
				  FROM        [DHRDataWarehouse].[dbo].DW_PR_ETAX X2
				  WHERE
					(X2.Loc_No = X.Loc_No)
					AND (X2.Emp_No = X.Emp_No)
					AND (X2.Wk_State = X.Wk_State)
					AND (X2.company = X.company)
					
					AND (X2.Eff_Date > X.Eff_Date)
					AND DATEDIFF(YEAR, X2.Eff_Date, GETDATE()) >= 2
				))
			  )
			  AND x.company = 21


			GROUP BY
			  x.company,
			  X.Loc_No,
			  X.Emp_No,
			  X.Wk_State,
			  X.Eff_Date,
			  E.pkUniqueId

			HAVING
			  COUNT(*) > 1
			  
			--ORDER BY X.Emp_No  ;
	) DUPS
	ON	   (
			DUPS.company = T1.company
	  AND	DUPS.Loc_No = T1.Loc_No
	  AND	DUPS.Emp_No = T1.Emp_No
			)
;



/* *** Check for Carrier #0 in ran WC *** */
UPDATE E SET FAILFLAG = FAILFLAG | 256 
--SELECT * 
FROM	#TEMP_EMP_VALCHK E
	INNER JOIN [DHRDataWarehouse].[dbo].DW_PR_WCOMP W ON (E.Loc_No = W.Loc_No 
													  AND E.Emp_No = W.Emp_No
													  AND E.company = W.company
													  )
WHERE W.Tp_No = 0
  AND Active_Stat <> 'T' -- only active employees
 ;

/* *** Check for Missing Ded Codes *** */  

/* SKIP FOR NOW ***
UPDATE T1 SET FAILFLAG = FAILFLAG | 1024
--SELECT *
FROM	#TEMP_EMP_VALCHK T1,  
		(SELECT
		  E.Loc_No,
		  E.Emp_No
		  --,'Ded '+ Convert(V.Code, SQL_VARCHAR)+IF(V.Plan is null, '', ' Plan "'+V.Plan+'"')+IF(V.Goal is not null, ' with goal of '+Convert(V.Goal, SQL_VarChar), '')+' must be setup for employees in Client '+Convert(E.Loc_No, SQL_VarChar)+'-:.d(%.2d)CurrentCompany'
		
		FROM	Summxtra.VFY_DedCodes V
		  inner join T1 on ((T1.Loc_No = V.Loc_No AND V.CompNo = T1.CurrentCompany)
											 OR V.CompNo = 0
											 )
		
		WHERE	V.ProcSetup = 0  
			AND	(V.CompNo = 0 OR V.CompNo = CurrentCompany)
			AND (V.Start_Date <= CurDate()
				 AND 
				 (V.Stop_Date is null OR V.Stop_Date >= CurDate())
				)
			--AND E.Active_Stat <> 'T'
			AND NOT EXISTS(
				SELECT	*
				FROM	APEX012016.PR_DED D
				WHERE	D.Loc_No = T1.Loc_No
				  AND	D.Emp_No = T1.Emp_No
				  AND	D.Code = V.Code
				  AND	D.Plan = IFNULL(V.Plan, D.Plan)
				  AND	IFNULL(D.Emp_Goal, 0) = IFNULL(V.Goal, IFNULL(D.Emp_Goal, 0))
				  AND	D.Active <> 'N'
				  AND	((D.Stop_Date is null) OR (D.Stop_Date >= CurDate()))
				  AND	((D.Start_Date is null) OR (D.Start_Date <= CurDate()))
			)
		)FOO
WHERE T1.Emp_No = FOO.Emp_No
  AND T1.Loc_No = FOO.Loc_No
;
*/

/* *** Check existing Ded Codes *** */

/* SKIP FOR NOW ***

UPDATE T1 SET FAILFLAG = FAILFLAG | 2048
FROM	#TEMP_EMP_VALCHK T1,  
		(
		
		SELECT
		  --:CurrentCompany as CompNo,
		  E.Loc_No,
		  E.Emp_No
		  --,'Ded '+Convert(V.Code, SQL_VARCHAR)+IF(V.Plan is null, '', ' Plan "'+V.Plan+'"')+IF(V.Goal is not null, ' with goal of '+Convert(V.Goal, SQL_VarChar), '')+' must be setup for employees in Client '+Convert(E.Loc_No, SQL_VarChar)+'-:.d(%.2d)CurrentCompany'


		FROM	Summxtra.VFY_DedCodes V
		  inner join APEX012016.PR_MAST E on
		  (
			((E.Loc_No = V.Loc_No) 
			--and (V.CompNo = :CurrentCompany)
			)
			or (V.CompNo = 0)
		  )

		WHERE	V.ProcSetup = 0
		  and	(--(V.CompNo = :CurrentCompany) or 
				(V.CompNo = 0)
				)
		  and	(V.Start_Date <= CurDate()
				and (
					V.Stop_Date is null
					or V.Stop_Date >= CurDate()
					)
				)
		  AND (E.Active_Stat <> 'T')
		  AND NOT EXISTS(
				SELECT	*
				FROM	APEX012016.PR_DED D
				WHERE	D.Loc_No = E.Loc_No
				  AND	D.Emp_No = E.Emp_No
				  AND	D.Code = V.Code
				  AND	D.Plan = IFNULL(V.Plan, D.Plan)
				  AND	(IFNULL(D.Emp_Goal, 0) = IFNULL(V.Goal, IFNULL(D.Emp_Goal, 0)))
				  AND	D.Active <> 'N'
				  AND	((D.Stop_Date is null) or (D.Stop_Date >= CurDate()))
				  AND	((D.Start_Date is null) or (D.Start_Date <= CurDate()))
				)
  
		)FOO
WHERE T1.Emp_No = FOO.Emp_No
  AND T1.Loc_No = FOO.Loc_No
  */
;



/* *** Exempt from FUTA = Y *** */
UPDATE T1 SET FAILFLAG = FAILFLAG | 4096
--SELECT *  
FROM	#TEMP_EMP_VALCHK T1  
WHERE	T1.Loc_No in (5497)
	AND	T1.Active_Stat <> 'T'
	AND	T1.Xmpt_Futa <> 'Y'
;

/* AIG - No policy number for EE Tax setup - AIGNOPOLICY */
UPDATE E SET FAILFLAG = FAILFLAG | 8192
/*
select
  e.company,
  E.Loc_No,
  E.Emp_No
  --'AIG - No policy number for EE Tax setup '+ET.Wc_State+' '+Convert(ET.Eff_Date, SQL_VARCHAR)+' WC State "'+ET.Wc_State+'".'
*/
FROM
  --[DHRDataWarehouse].[dbo].DW_CL_Mast C
  --inner join 
  #TEMP_EMP_VALCHK E
  inner join [DHRDataWarehouse].[dbo].DW_PR_ETAX ET on
  (
    (ET.Loc_No=E.Loc_No)
    and (ET.Emp_No=E.Emp_No)
	and (ET.company=E.company)
    and (ET.Eff_Date in (
			select	Max(ET2.Eff_Date)
			from	[DHRDataWarehouse].[dbo].DW_PR_ETAX ET2
			where	(ET2.Loc_No=ET.Loc_No)
			  and	(ET2.Emp_No=ET.Emp_No)
			  and	(ET2.Wk_State=ET.Wk_State)
			  and   (ET2.company=ET.company)
    ))
  )
  inner join [DHRDataWarehouse].[dbo].DW_SX_CarrierMap Mp on
  (
    (Mp.CarrierNo=ET.Tpay_No_4)
    --and (Mp.CompNo=:CurrentCompany)
	and (Mp.company=ET.company)
    and (
      (Mp.StartDate is null)
      or (
        (Mp.StartDate in (
			select	Max(CM2.StartDate)
			from	[DHRDataWarehouse].[dbo].DW_SX_CarrierMap  CM2
			where	(CM2.company = Mp.company)
			  and	(CM2.CarrierNo = Mp.CarrierNo)
        ))
      )
    )
  )
  left outer join [DHRDATAWAREHOUSE].[DBO].DW_SX_AIG_POLICY P on
  (
    (P.[State] = ET.Wc_State)
    and (
      (P.IsStateMaster = 'Y')
      or
      (
        exists(
          select	*
          from	[DHRDataWarehouse].[dbo].DW_SX_AIG_Policy_Client PC
          where	(PC.PolicyNo = P.PolicyNo)
            --and (PC.CompNo = :CurrentCompany)
			and (PC.company = e.company)
            and (PC.Loc_No = E.Loc_No)
        )
      )
    )
  )

where	(E.C_Delete_Date is null)
  and	(E.Active_Stat <> 'T')
  --and	(E.Last_Paid is not null)

  and	((P.PolicyNo is null) or (Len(RTrim(P.PolicyNo)) = 0))
  
  and	(Mp.Carrier = 'AIG')
  
  
  and	(exists(
			select	*
			from	#TEMP_EMP_VALCHK EC
			where	(EC.Last_Paid is not null)
			  and	(EC.Loc_No = E.Loc_No)
			  and	(EC.company = E.company)
		  ))
  
  and	(exists(
			select	*
			from	[DHRDataWarehouse].[dbo].DW_PR_WCOMP  W
			where	(W.Loc_No = ET.Loc_No)
			  and	(W.Emp_No = ET.Emp_No)
			  and	(W.[State] = ET.Wc_State)
			  and	(W.company = ET.company)
			  and	[year] = year(getdate())
		  ))
--order by company, Emp_No

;


/* Invalid WC Code for Scheduled Pay Pay Code - WCINVALID*/
UPDATE E SET FAILFLAG = FAILFLAG | 16384
/*
SELECT
  --:CurrentCompany as CompNo,
  E.company,
  E.Loc_No,
  E.Emp_No
  --'Invalid WC Code for Scheduled Pay Pay Code',
  --'Pay Code '+Convert(S.Code, SQL_VARCHAR)+' WC Code "'+S.Wc_Code+'"'
*/
FROM	#TEMP_EMP_VALCHK	E
  --inner join [DHRDataWarehouse].[dbo].DW_CL_MAST C on  (E.Loc_No = C.Loc_No and E.company = C.company )
  inner join [DHRDataWarehouse].[dbo].DW_PR_SPAY S on  (S.Loc_No = E.Loc_No and S.Emp_No = E.Emp_No and S.company = E.company)

where	E.C_Delete_Date is null
  and	S.Wc_Code < '9900'
  and	S.Code in (112, 107, 109, 148, 200)
  and	((S.Stop_Date is null) or (S.Stop_Date > getDate()))
  and	E.Active_Stat <> 'T'

--order by  company, Loc_No, Emp_No
;

/* Deduction Not in Master or Client Approved - NODEDUCTION */
UPDATE #TEMP_EMP_VALCHK SET FAILFLAG = FAILFLAG | 32768
--SELECT	E.company,		E.Loc_No,		E.Emp_No
FROM #TEMP_EMP_VALCHK E 
	INNER JOIN (
		/* get the employees with NO dedcutions */
		SELECT DISTINCT

		  D.company,
		  D.Loc_No,
		  D.Emp_No
		  --'Deduction Not in Master or Client Approved',
		  --'CODE '+Convert(D.Code, SQL_VARCHAR)+' PLAN "'+D.Plan+'"'
  
		FROM  [DHRDataWarehouse].[dbo].DW_PR_DED D

		WHERE
		  (D.Active <> 'N')
		  AND 
			(
			  NOT EXISTS(
				SELECT	*
				FROM	[DHRDataWarehouse].[dbo].DW_PR_MDED M
				WHERE	M.Code = D.Code
				AND		M.[Plan] = D.[Plan]
				AND		M.company = D.company
				--and		M.company = E.company
				
			)
			OR 
			(
			  NOT EXISTS(
				SELECT	*
				FROM	[DHRDataWarehouse].[dbo].DW_CL_Deductions CD
				WHERE	CD.Loc_No = D.Loc_No
				  AND	CD.Code = D.Code
				  AND	CD.[Plan] = D.[Plan]
				  AND	CD.company = D.company
				  --and CD.company = E.company
			  )
			)
		  )

		  --ORDER BY   company,Emp_No, Loc_No
		) foo
		ON  (E.Loc_No = foo.Loc_No AND E.Emp_No = foo.Emp_No AND E.company = foo.company)

WHERE	(E.C_Delete_Date IS NULL)
	AND (E.Active_Stat <> 'T')

--ORDER BY   company,Emp_No, Loc_No
;






/* Address contains invalid characters - BADADDRESS */

UPDATE T1 SET FAILFLAG = FAILFLAG | 4194304
--SELECT pkUniqueID, Addr1, Addr2, City, State, Zip AS zipCode, mod_date
FROM #TEMP_EMP_VALCHK T1
WHERE 
	--(Addr1 like '%[,!@$%&*()_+=\^/'']%'	or	Addr2 like '%[,!@$%&*()_+=\^/'']%'	or	City like '%[1234567890#,!@$%&*()_+=\^/]%'	)
	(Addr1 like '%[,]%'	or	Addr2 like '%[,]%'	or	City like '%[,]%'	)
--ORDER BY pkUniqueID
;
  
/* Address zip is invalid - BADADDRESS */
UPDATE T1 SET FAILFLAG = FAILFLAG | 4194304	--SAME CODE AS ABOVE
FROM #TEMP_EMP_VALCHK T1
JOIN (

	SELECT * FROM
	(
		SELECT pkUniqueID, Addr1, Addr2, City, State, LTRIM(RTRIM(Zip)) AS zipCode, mod_date
		FROM #TEMP_EMP_VALCHK 
		--WHERE CONVERT(date, mod_date) = CONVERT(date, getDate())

	) foo
	WHERE	(
				zipCode is not null
			AND zipCode != ''
			AND (LEN(zipCode)  < 5
				OR  
				zipCode not LIKE '%[0-9]%')
			)
	) f ON f.pkUniqueID = T1.pkUniqueID
;	

/* Address State is invalid - BADADDRESS */
UPDATE T1 SET FAILFLAG = FAILFLAG | 4194304
FROM	#TEMP_EMP_VALCHK T1
WHERE	State NOT IN (SELECT abbreviation FROM dbo.States)
	AND State != null
	AND	State is not null





	/* END TODO: Insert the queires below */

	/* delete records from today */
	DELETE FROM DW_VALIDATIONCHECKS_EMPLOYEES WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());

	/* 
		Insert records from temp table into history table.
		Note that it only inserts the records that failed validation
	*/
	INSERT INTO DW_VALIDATIONCHECKS_EMPLOYEES
	SELECT	* 
	FROM	#TEMP_EMP_VALCHK
	WHERE	FAILFLAG != 0;

	--SELECT * FROM DW_VALIDATIONCHECKS_EMPLOYEES WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());
	SELECT count(*) as RecordCount FROM DW_VALIDATIONCHECKS_EMPLOYEES WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());

END

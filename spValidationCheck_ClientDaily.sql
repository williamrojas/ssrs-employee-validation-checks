USE [DHRDataWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[spValidationCheck_ClientDaily]    Script Date: 9/8/2016 10:28:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidationCheck_ClientDaily]

AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
		SET NOCOUNT ON;

    
		SELECT	C.*, 
				0 AS FAILFLAG
				,GETDATE() as mod_date
		INTO	#TEMP_CLIENT_VALCHK
		--FROM	APEX012016.CL_Mast C
		FROM	[DHRDataWarehouse].[dbo].DW_CL_Mast C 
		WHERE 	C.loc_No >= 1000
		  --AND 	C.Delete_Date is null
		;




		/* *** Missing Client EIN / 'NO EIN' -- NOEIN  *** */
		UPDATE T1 SET FAILFLAG = FAILFLAG | 32 
		--SELECT	*	
		FROM	#TEMP_CLIENT_VALCHK T1 
		WHERE	Delete_Date is null
				AND (RTrim(Ein) = ''
				or 
    			Ein = 'NONE LISTED')
		--ORDER BY company
		;		
					

		/* Paytech 
		NOPAYTECHINCLIENT	= 64
		NOPAYTECH			= 128
		*/
		UPDATE T SET FAILFLAG = FAILFLAG | foo.FF
		--SELECT * 
		FROM	#TEMP_CLIENT_VALCHK T,
			(
			SELECT T1.pkClientID
					--T1.company,		T1.loc_no,		C2.Pay_Tech		,R.[Init]
					,CASE
								WHEN ((C2.Pay_Tech is null) or (RTrim(C2.Pay_Tech)=''))	THEN 64
								WHEN ((R.[Init] is null) or (RTrim(R.[Init]) = '')) 	THEN 128
								ELSE	0
					END as FF 

			FROM	#TEMP_CLIENT_VALCHK T1
				left outer join [DHRDataWarehouse].[dbo].DW_CL_Mast2 C2 on  (C2.Loc_No = T1.Loc_No and C2.company = T1.company)
				left outer join [DHRDataWarehouse].[dbo].DW_CL_Rep2  R  on  ((R.[Init]=C2.Pay_Tech) and (R.[Type]='P'))

			WHERE	T1.Delete_Date is null
				AND (C2.Pay_Tech is null 	
					OR	RTrim(C2.Pay_Tech) = ''
					OR	R.[Init] is null		 
					OR	RTrim(R.[Init]) = ''
					)

			--ORDER BY R.[Init]
		) FOO
		WHERE  T.pkClientID =  FOO.pkClientID
		;



		/* *** No Carrier setup. Check that Carrier # is greater than 0' as Validation *** */
		UPDATE T1 SET FAILFLAG = FAILFLAG | 512 
		--select *
		FROM	#TEMP_CLIENT_VALCHK T1
		WHERE	T1.Delete_Date is null
		  AND	not exists(
			select * 
			from (
				SELECT	company,
						Loc_No,
						Div_No,
						Work_State,
						Eff_Date
						,RANK() OVER (PARTITION BY company, Loc_No, div_no, Work_State ORDER BY Eff_Date desc) AS RNK
						--,RANK() OVER (PARTITION BY company, Loc_No, Work_State ORDER BY Eff_Date desc) AS RNK

				FROM	[DHRDataWarehouse].[dbo].DW_CL_SDEF 
				where	Wc_Tp_No > 0

				) foo
			where rnk = 1
			--ORDER BY company, Loc_No, Work_State 
			)
		;



		/* AIG - Overlapping Policy Dates -- AIGPOLICYOVERLAP */ 
		UPDATE T1 SET FAILFLAG = FAILFLAG | 65536 
		--SELECT * 
		FROM #TEMP_CLIENT_VALCHK T1
		INNER JOIN (
		select distinct  
		  PC.company,
		  PC.Loc_No
		  --,'AIG - Overlapping Policy Dates '+ RTrim(P.State)    + ' ' + RTrim(P.PolicyNo)    + ' ' + Convert(P.PolicyStart, SQL_VARCHAR) + ' through ' + Convert(P.PolicyEnd, SQL_VARCHAR)

		FROM			[DHRDATAWAREHOUSE].[DBO].DW_SX_AIG_POLICY P
		  INNER JOIN	[DHRDataWarehouse].[dbo].DW_SX_AIG_Policy_Client PC ON  (PC.PolicyNo = P.PolicyNo)
  
		WHERE  (P.IsStateMaster is null or P.IsStateMaster = ' ')
		  AND	(
				EXISTS(
					SELECT	*
					FROM	[DHRDATAWAREHOUSE].[DBO].DW_SX_AIG_POLICY P1
					  INNER JOIN [DHRDATAWAREHOUSE].[DBO].DW_SX_AIG_Policy_Client PC1 ON	(PC1.PolicyNo = P1.PolicyNo)
			  
					WHERE	PC1.company = PC.company
					  AND	PC1.Loc_No = PC.Loc_No
					  AND	P1.State = P.State
					  AND	PC1.PolicyNo <> PC.PolicyNo
					  AND	(
							PC1.StartDate <= PC.StopDate
							AND 
							PC1.StopDate >= PC.StartDate
							)
					)
				)
			)PO
			ON (T1.company = PO.company and T1.Loc_No = PO.Loc_No)
		;


		/* Client has both Client and Division schedules -- DIVSCHEDULES */
		UPDATE C SET FAILFLAG = FAILFLAG | 131072
		--SELECT	C.company,	C.Loc_No
		FROM	#TEMP_CLIENT_VALCHK C
		WHERE	C.Delete_Date is null
		  AND	EXISTS(
				select	*
				from	[DHRDATAWAREHOUSE].[DBO].DW_CL_Schedule S
				where	S.company = C.company
				  AND   S.Loc_No = C.Loc_No
				  AND	S.Div_No <> 0
				  AND	EXISTS(
								SELECT	*
								FROM	[DHRDATAWAREHOUSE].[DBO].DW_CL_Schedule S2
								WHERE	S2.company = S.company
								  AND   S2.Loc_No = S.Loc_No
								  AND	S2.Div_No = 0
							)
				)
		;

		/* Has unused schedule either for a termed division or client. -- UNUSEDSCHEDULE*/
		UPDATE C SET FAILFLAG = FAILFLAG | 262144			
		--SELECT *
		  --C.company,  C.Loc_No  --,'Has unused schedule either for a termed division or client.'
  
		--FROM	[DHRDataWarehouse].[dbo].DW_CL_Mast C
		FROM	#TEMP_CLIENT_VALCHK C

		WHERE  (EXISTS(
				SELECT	*
				FROM	[DHRDataWarehouse].[dbo].DW_CL_Scheduled S
				WHERE	S.company = C.company
				  AND	S.[year] = year(getdate()) 
				  AND	S.Loc_No = C.Loc_No
				  AND	S.Pay_Date >= GetDate()
				  AND(	
						EXISTS (
							SELECT	*
							FROM	[DHRDataWarehouse].[dbo].DW_CL_MAST CX
							WHERE	(CX.company = S.company)
							  AND	(CX.Loc_No = S.Loc_No)
							  AND	(CX.Delete_Date IS NOT NULL)
							)
						OR
						EXISTS (
							SELECT	*
							FROM	[DHRDataWarehouse].[dbo].DW_CL_Divisions D
							WHERE	(D.Delete_Date IS NOT NULL)
							  AND	(D.company = S.company)
							  AND	(D.Loc_No = S.Loc_No)
							  AND	(D.Div_No = S.Div_No)
				)
			  )
		   ))
		   --ORDER BY company		
		;   
	
		/* DUPLICATEEFFORTERM */
		UPDATE C SET FAILFLAG = FAILFLAG | 524288
		--select C.company,  C.Loc_No
		  --,'Client Status CHG, Duplicate Eff or Term date. Correct with Client Lookup "Status Chg" button.'

		--FROM	[DHRDataWarehouse].[dbo].DW_CL_Mast C
		FROM	#TEMP_CLIENT_VALCHK C

		WHERE  exists(
			SELECT	*
			FROM	[DHRDataWarehouse].[dbo].[DW_SX_CL_SCHG] X
			WHERE	(X.company = C.company)
			  AND (X.Loc_No = C.Loc_No)
			  AND (X.Dupe = 'Y')
		  )
		;

		/* INVOICEANDPAYDATES */  
		UPDATE C SET FAILFLAG = FAILFLAG | 1048576
		--SELECT  C.company,  I.Loc_No,  I.Div_No
  
		  --,'Invoice Date and Pay Date are different'
		  --,'Invoice Date is '+Convert(I.Inv_Date, SQL_VARCHAR)+', Pay Date is "'+Convert(I.Pay_Date, SQL_VARCHAR)+'"'

		--FROM	[DHRDataWarehouse].[dbo].DW_CL_MAST C
		FROM	#TEMP_CLIENT_VALCHK C
		  inner join [DHRDataWarehouse].[dbo].DW_PR_INVOI I on  (I.company = C.company AND I.Loc_No = C.Loc_No)

		WHERE  C.Delete_Date is null
		  AND	I.[year] = year(getdate())
		  AND I.Pay_Date <> I.Inv_Date
		  AND (
				I.Pay_Date >= dateAdd(year, -7, GetDate())
				or 
				I.Inv_Date >= dateAdd(year, -7, GetDate())
			  )
		  AND	I.[year] = year(getdate()) 
		;


		/* More than one Client Address (EE Verify) -- DUPADDRESS*/
		UPDATE C SET FAILFLAG = FAILFLAG | 2097152
		/*
		select  C.company,  C.Loc_No
		  --,'More than one Client Address (EE Verify)'
		  */
		--from	[DHRDataWarehouse].[dbo].DW_CL_Mast C
		FROM	#TEMP_CLIENT_VALCHK C
		where	C.Delete_Date is null
		  --and	C.Loc_No >= 1000
		  and	exists(
					select	*
					from	[DHRDataWarehouse].[dbo].DW_CL_ADDR A
					where	A.Type = 1
						and	A.Loc_No = C.Loc_No
						and	A.company = C.company
						and	A.Div_No <> 999
						and	exists(
								select	*
								from	[DHRDataWarehouse].[dbo].DW_CL_ADDR A2
								where	A2.Loc_No = A.Loc_No
									and	A2.company = A.company
									and	A2.Div_No = A.Div_No
									and	A2.Type = A.Type
									and	A2.pkUniqueId <> A.pkUniqueID
								)
						)
		;

		/* BADADDRESS */
		UPDATE C SET FAILFLAG = FAILFLAG | 8388608
		--SELECT DISTINCT *
		--SELECT Addr1, Addr2, City, State, Zip AS zipCode
		FROM	#TEMP_CLIENT_VALCHK C
			JOIN [DHRDataWarehouse].[dbo].DW_CL_ADDR A ON (A.Loc_No = C.Loc_No
														and	A.company = C.company
														and	C.Div_No = A.Div_No
															)
		WHERE	C.Delete_Date is null
			AND A.Type = 1
			AND	A.Div_No <> 999
			--AND (Addr1 like '%[,!@$%&*()_+=\^/'']%' OR	Addr2 like '%[,!@$%&*()_+=\^/'']%' OR	City like '%[1234567890#,!@$%&*()_+=\^/]%')
			AND (Addr1 like '%[,]%' OR	Addr2 like '%[,]%' OR City like '%[,]%')
		;

		/* zip code -- BADADDRESS */
		UPDATE T1 SET FAILFLAG = FAILFLAG | 8388608
		--SELECT *
		FROM #TEMP_CLIENT_VALCHK T1
		JOIN (
			SELECT * FROM (

				SELECT C.pkClientID, A.Addr1, A.Addr2, A.City, A.State, LTRIM(RTRIM(A.Zip)) AS zipCode
				FROM	#TEMP_CLIENT_VALCHK C
					JOIN [DHRDataWarehouse].[dbo].DW_CL_ADDR A ON (A.Loc_No = C.Loc_No
																and	A.company = C.company
																and	C.Div_No = A.Div_No
																	)
				WHERE A.Type = 1
			)foo
			WHERE	(
				zipCode is not null
			AND zipCode != ''
			AND (LEN(zipCode)  < 5
				OR  
				zipCode not LIKE '%[0-9]%')
				)
	) f ON f.pkClientID = T1.pkClientID
		
	/* END TODO: Insert the queries below */

	/* delete records from today */
	
	DELETE FROM DW_VALIDATIONCHECKS_CLIENT WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());

	/* 
		Insert records from temp table into history table.
		Note that it only inserts the records that failed validation
	*/
	
	INSERT INTO DW_VALIDATIONCHECKS_CLIENT
	SELECT	* 
	FROM	#TEMP_CLIENT_VALCHK
	WHERE	FAILFLAG != 0;
	
	--SELECT * FROM DW_VALIDATIONCHECKS_CLIENT WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());
	SELECT count(*) as RecordCount FROM DW_VALIDATIONCHECKS_CLIENT WHERE CONVERT(date, mod_date) = CONVERT(date, getDate());



	
END

USE [DHRDataWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[spValidationCheck_ClientCreateReportTable]    Script Date: 9/8/2016 10:28:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidationCheck_ClientCreateReportTable]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if not exists (select * from sysobjects where name='DW_VALIDATIONCHECKS_CLIENT' AND type = 'U')
		BEGIN



		

		CREATE TABLE [dbo].[DW_VALIDATIONCHECKS_CLIENT](
			[pkClientID] [int] NOT NULL,
			[company] [int] NOT NULL,
			[Loc_No] [int] NOT NULL,
			[Div_No] [int] NULL,
			[SummitID] [int] NOT NULL,
			[Name] [varchar](40) NOT NULL,
			[Dba_Name] [varchar](40) NULL,
			[Short_Name] [varchar](15) NULL,
			[fkRepID] [int] NULL,
			[Home_St] [varchar](2) NULL,
			[Client_Date] [datetime] NULL,
			[Delete_Date] [datetime] NULL,
			[Next_No] [int] NOT NULL,
			[Bank_No] [int] NULL,
			[Renewal_Date] [datetime] NULL,
			[Fedx_No] [varchar](20) NULL,
			[Employees] [int] NOT NULL,
			[GrossPay] [decimal](14, 2) NULL,
			[Bus_Type] [varchar](6) NULL,
			[Bus_Since_Date] [datetime] NULL,
			[Corp_Type] [char](1) NULL,
			[Chkfont_Cust] [varchar](8) NULL,
			[Ft_Hrs_Week] [int] NOT NULL,
			[Alloc_Sales] [decimal](14, 2) NOT NULL,
			[County_No] [varchar](5) NULL,
			[County_Name] [varchar](20) NULL,
			[Comment] [varchar](122) NULL,
			[Inv_Comment] [varchar](70) NULL,
			[No_Divisions] [int] NULL,
			[Yn_1] [char](1) NULL,
			[Yn_2] [char](1) NULL,
			[Yn_3] [char](1) NULL,
			[Yn_4] [char](1) NULL,
			[Yn_5] [char](1) NULL,
			[Yn_6] [char](1) NULL,
			[Yn_7] [char](1) NULL,
			[Yn_8] [char](1) NULL,
			[Yn_9] [char](1) NULL,
			[Yn_10] [char](1) NULL,
			[Yn_11] [char](1) NULL,
			[Yn_12] [char](1) NULL,
			[Yn_13] [char](1) NULL,
			[Yn_14] [char](1) NULL,
			[Yn_15] [char](1) NULL,
			[Yn_16] [char](1) NULL,
			[Yn_17] [char](1) NULL,
			[Yn_18] [char](1) NULL,
			[Yn_19] [char](1) NULL,
			[Yn_20] [char](1) NULL,
			[Yn_21] [char](1) NULL,
			[Yn_22] [char](1) NULL,
			[Yn_23] [char](1) NULL,
			[Yn_24] [char](1) NULL,
			[Yn_25] [char](1) NULL,
			[Yn_26] [char](1) NULL,
			[Yn_27] [char](1) NULL,
			[Yn_28] [char](1) NULL,
			[Yn_29] [char](1) NULL,
			[Yn_30] [char](1) NULL,
			[Yn_31] [char](1) NULL,
			[Yn_32] [char](1) NULL,
			[Yn_33] [char](1) NULL,
			[Yn_34] [char](1) NULL,
			[Yn_35] [char](1) NULL,
			[Yn_36] [char](1) NULL,
			[Yn_37] [char](1) NULL,
			[Yn_38] [char](1) NULL,
			[Yn_39] [char](1) NULL,
			[Yn_40] [char](1) NULL,
			[Yn_41] [char](1) NULL,
			[Yn_42] [char](1) NULL,
			[Yn_43] [char](1) NULL,
			[Yn_44] [char](1) NULL,
			[Yn_45] [char](1) NULL,
			[Yn_46] [char](1) NULL,
			[Yn_47] [char](1) NULL,
			[Yn_48] [char](1) NULL,
			[Yn_49] [char](1) NULL,
			[Yn_50] [char](1) NULL,
			[Yn_51] [char](1) NULL,
			[Yn_52] [char](1) NULL,
			[Yn_53] [char](1) NULL,
			[Yn_54] [char](1) NULL,
			[Yn_55] [char](1) NULL,
			[Yn_56] [char](1) NULL,
			[Yn_57] [char](1) NULL,
			[Yn_58] [char](1) NULL,
			[Yn_59] [char](1) NULL,
			[Yn_60] [char](1) NULL,
			[Yn_61] [char](1) NULL,
			[Yn_62] [char](1) NULL,
			[Yn_63] [char](1) NULL,
			[Yn_64] [char](1) NULL,
			[Yn_65] [char](1) NULL,
			[Yn_66] [char](1) NULL,
			[Yn_67] [char](1) NULL,
			[Yn_68] [char](1) NULL,
			[Yn_69] [char](1) NULL,
			[Yn_70] [char](1) NULL,
			[Yn_71] [char](1) NULL,
			[Yn_72] [char](1) NULL,
			[Yn_73] [char](1) NULL,
			[Yn_74] [char](1) NULL,
			[Yn_75] [char](1) NULL,
			[Yn_76] [char](1) NULL,
			[Yn_77] [char](1) NULL,
			[Yn_78] [char](1) NULL,
			[Yn_79] [char](1) NULL,
			[Yn_80] [char](1) NULL,
			[Yn_81] [char](1) NULL,
			[Yn_82] [char](1) NULL,
			[Yn_83] [char](1) NULL,
			[Yn_84] [char](1) NULL,
			[Yn_85] [char](1) NULL,
			[Yn_86] [char](1) NULL,
			[Yn_87] [char](1) NULL,
			[Yn_88] [char](1) NULL,
			[Yn_89] [char](1) NULL,
			[Yn_90] [char](1) NULL,
			[Plan_401k_1] [varchar](8) NULL,
			[Plan_401k_2] [varchar](8) NULL,
			[Start_401k_1] [datetime] NULL,
			[Start_401k_2] [datetime] NULL,
			[Stop_401k_1] [datetime] NULL,
			[Stop_401k_2] [datetime] NULL,
			[Jc_Titles_1] [varchar](10) NULL,
			[Jc_Titles_2] [varchar](10) NULL,
			[Jc_Titles_3] [varchar](10) NULL,
			[Jc_Titles_4] [varchar](10) NULL,
			[Jc_Type_1] [char](1) NULL,
			[Jc_Type_2] [char](1) NULL,
			[Jc_Type_3] [char](1) NULL,
			[Jc_Type_4] [char](1) NULL,
			[Jc_Length_1] [int] NULL,
			[Jc_Length_2] [int] NULL,
			[Jc_Length_3] [int] NULL,
			[Jc_Length_4] [int] NULL,
			[Jc_Cert] [char](1) NULL,
			[Jc_Format] [varchar](14) NULL,
			[Gl_Cfg_Meth] [int] NULL,
			[Gl_Corp_No] [varchar](2) NULL,
			[Gl_Dept] [varchar](2) NULL,
			[Inv_Fmt] [int] NULL,
			[Fed_Tp_No] [int] NULL,
			[Affiliate] [int] NULL,
			[Ucode_1] [varchar](3) NULL,
			[Ucode_2] [varchar](3) NULL,
			[Ucode_3] [varchar](3) NULL,
			[Ucode_4] [varchar](3) NULL,
			[Ucode_5] [varchar](3) NULL,
			[Ucode_6] [varchar](3) NULL,
			[Ucode_7] [varchar](3) NULL,
			[Ucode_8] [varchar](3) NULL,
			[Fee_Per_Inv] [decimal](14, 2) NULL,
			[Fee_Min_Inv] [decimal](14, 2) NULL,
			[Fee_Max_Inv] [decimal](14, 2) NULL,
			[Fee_Per_Emp_1] [decimal](14, 2) NULL,
			[Fee_Per_Emp_2] [decimal](14, 2) NULL,
			[Fee_Per_Emp_3] [decimal](14, 2) NULL,
			[Fee_Per_Emp_4] [decimal](14, 2) NULL,
			[Fee_Rate_1] [decimal](12, 4) NULL,
			[Fee_Rate_2] [decimal](12, 4) NULL,
			[Fee_Rate_3] [decimal](12, 4) NULL,
			[Fee_Max_Ytd] [decimal](14, 2) NULL,
			[Fee_Max_Chk] [decimal](14, 2) NULL,
			[Show_Adj_1] [char](1) NULL,
			[Show_Adj_2] [char](1) NULL,
			[Show_Adj_3] [char](1) NULL,
			[Show_Adj_4] [char](1) NULL,
			[Alloc_Pct] [int] NULL,
			[Ein] [varchar](11) NULL,
			[Reg_Pcode] [int] NULL,
			[Ot_Pcode] [int] NULL,
			[Sal_Pcode] [int] NULL,
			[Hol_Pay_Code] [int] NULL,
			[Cif_Code] [varchar](10) NULL,
			[Done_1] [char](1) NULL,
			[Done_2] [char](1) NULL,
			[Done_3] [char](1) NULL,
			[Done_4] [char](1) NULL,
			[Done_5] [char](1) NULL,
			[Done_6] [char](1) NULL,
			[Done_7] [char](1) NULL,
			[Done_8] [char](1) NULL,
			[Done_9] [char](1) NULL,
			[Done_10] [char](1) NULL,
			[Done_11] [char](1) NULL,
			[Done_12] [char](1) NULL,
			[Done_13] [char](1) NULL,
			[Done_14] [char](1) NULL,
			[Done_15] [char](1) NULL,
			[Done_16] [char](1) NULL,
			[Done_17] [char](1) NULL,
			[Done_18] [char](1) NULL,
			[Done_19] [char](1) NULL,
			[Done_20] [char](1) NULL,
			[Done_21] [char](1) NULL,
			[Done_22] [char](1) NULL,
			[Done_23] [char](1) NULL,
			[Done_24] [char](1) NULL,
			[Done_25] [char](1) NULL,
			[Proc_Order] [int] NULL,
			[Addl_Chk_Fee_1] [decimal](14, 2) NULL,
			[Addl_Chk_Fee_2] [decimal](14, 2) NULL,
			[Addl_Chk_Fee_3] [decimal](14, 2) NULL,
			[Fty_Per_Emp_1] [char](1) NULL,
			[Fty_Per_Emp_2] [char](1) NULL,
			[Fty_Per_Emp_3] [char](1) NULL,
			[Fty_Per_Emp_4] [char](1) NULL,
			[Addl_Chk_Fty_1] [char](1) NULL,
			[Addl_Chk_Fty_2] [char](1) NULL,
			[Addl_Chk_Fty_3] [char](1) NULL,
			[Addl_Chk_Fty_4] [char](1) NULL,
			[Show_Meth] [char](1) NULL,
			[Bank_Ddep] [int] NULL,
			[Show_Font] [char](1) NULL,
			[Sig_Font] [varchar](8) NULL,
			[Dbl_Pcode] [int] NULL,
			[Crdt_125] [char](1) NULL,
			[Bill_Zero] [char](1) NULL,
			[Timesheetfmt] [int] NULL,
			[Futarate] [decimal](14, 2) NULL,
			[Billfuta] [char](1) NULL,
			[Billsuta] [char](1) NULL,
			[Billfica] [char](1) NULL,
			[Invlogo] [varchar](12) NULL,
			[Extra] [varchar](77) NULL,
			[Rep] [varchar](3) NULL,
			[FAILFLAG] [int] NOT NULL,
			[mod_date] [datetime] NOT NULL
		) ON [PRIMARY]

























		RETURN 1;
		END
		
	ELSE
		RETURN -1;
	
END

USE [DHRDataWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[spValidationCheck_EmployeesCreateReportTable]    Script Date: 9/8/2016 10:28:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidationCheck_EmployeesCreateReportTable]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--drop table DW_VALIDATIONCHECKS_EMPLOYEES
	if not exists (select * from sysobjects where name='DW_VALIDATIONCHECKS_EMPLOYEES' AND type = 'U')
	BEGIN

    SELECT	TOP 0 E.*, 
			C.Delete_Date as C_Delete_Date,
			0 AS FAILFLAG,
			GETDATE() as mod_date

	INTO	DW_VALIDATIONCHECKS_EMPLOYEES	

	FROM	[DHRDataWarehouse].[dbo].DW_PR_Mast E    
		JOIN	[DHRDataWarehouse].[dbo].DW_CL_Mast C on (E.Loc_No = C.Loc_No AND E.company = C.company)  
   
	WHERE C.loc_No >= 1000
	--AND C.Delete_Date is null
	--AND E.Active_Stat <> 'T' -- only active employees
	;
	END
END
